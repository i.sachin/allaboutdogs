//
//  DataState.swift
//  AllAboutDogs
//
//  Created by Sachin Sawant on 10/7/19.
//  Copyright © 2019 Sachin Sawant. All rights reserved.
//

import Foundation

enum DataState {
    case loading
    case loaded
    case error
    case empty
}
