//
//  APIError.swift
//  AllAboutDogs
//
//  Created by Sachin Sawant on 10/7/19.
//  Copyright © 2019 Sachin Sawant. All rights reserved.
//

import Foundation

enum APIError: Error {
    case noNetwork
    case networkFailure
    case parseError
    case unknown
}
