//
//  DogAPIProvidable.swift
//  AllAboutDogs
//
//  Created by Sachin Sawant on 11/7/19.
//  Copyright © 2019 Sachin Sawant. All rights reserved.
//

import Foundation

protocol DogAPIProvidable {
    func makeDogAPI() -> DogAPI
}

struct DefaultDogAPIProvider: DogAPIProvidable {
    func makeDogAPI() -> DogAPI {
        return DefaultDogAPI()
    }
}
