//
//  DogAPI.swift
//  AllAboutDogs
//
//  Created by Sachin Sawant on 10/7/19.
//  Copyright © 2019 Sachin Sawant. All rights reserved.
//

import Foundation

protocol DogAPI {
    func fetchDogCollection(sourceURL: URL, completion: @escaping (Result<DogCollection, APIError>) -> Void)
}

class DefaultDogAPI: DogAPI {
    
    func fetchDogCollection(sourceURL: URL, completion: @escaping (Result<DogCollection, APIError>) -> Void) {
        var request = URLRequest(url: sourceURL)
        request.httpMethod = "GET"
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if let jsonData = data{
                if let dogCollection = try? JSONDecoder().decode(DogCollection.self, from: jsonData) {
                    completion(.success(dogCollection))
                } else {
                    completion(.failure(.parseError))
                }
            }
            else {
                completion(.failure(.networkFailure))
            }
        }
        task.resume()
    }
}
