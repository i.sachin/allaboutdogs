//
//  TitledTextView.swift
//  AllAboutDogs
//
//  Created by Sachin Sawant on 11/7/19.
//  Copyright © 2019 Sachin Sawant. All rights reserved.
//

import UIKit
import SnapKit

class TitledTextView: UIView {
    
    public var title: String? {
        didSet {
            titleLabel.text = title
        }
    }
    
    public var text: String? {
        didSet {
            textLabel.text = text
        }
    }
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.textAlignment = .left
        label.numberOfLines = 1
        label.textColor = UIColor.darkGray
        return label
    }()
    
    private lazy var textLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.textAlignment = .left
        label.numberOfLines = 0
        return label
    }()
    
    init() {
        super.init(frame: CGRect.zero)
        
        addSubview(titleLabel)
        addSubview(textLabel)
        
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(8)
            make.top.equalToSuperview().offset(5)
        }
        
        textLabel.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(3)
            make.left.equalToSuperview().offset(8)
            make.right.equalToSuperview().offset(-8)
            make.bottom.equalToSuperview().offset(-5)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
