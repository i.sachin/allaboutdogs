//
//  DogTableViewCell.swift
//  AllAboutDogs
//
//  Created by Sachin Sawant on 10/7/19.
//  Copyright © 2019 Sachin Sawant. All rights reserved.
//

import UIKit

class DogTableViewCell: UITableViewCell {

    static let reuseIdentifier: String = String(describing: self)
    
    public var name: String = "" {
        didSet {
            nameView.text = name
            nameLabel.text = name
        }
    }
    
    public var lifespan: String = "" {
        didSet {
            lifespanView.text = lifespan
        }
    }
    
    public var speciality: String = "" {
        didSet {
            specialityView.text = speciality
        }
    }
    
    public var imageSize: CGSize = CGSize(width: 0, height: 0) {
        didSet {
            dogImageView.snp.remakeConstraints { make in
                make.top.equalToSuperview().offset(16)
                make.left.right.equalToSuperview()
                make.width.equalToSuperview()
                make.height.equalTo(UIScreen.main.bounds.width * imageSize.height / imageSize.width )
            }
            layoutIfNeeded()
        }
    }
    
    public lazy var dogImageView: UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "dog-placeholder"))
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 24)
        label.textAlignment = .center
        return label
    }()
    
    private lazy var nameView: TitledTextView = {
        let nameView = TitledTextView()
        nameView.title = "Name:"
        return nameView
    }()
    
    private lazy var lifespanView: TitledTextView = {
        let lifespanView = TitledTextView()
        lifespanView.title = "Lifespan:"
        return lifespanView
    }()
    
    private lazy var specialityView: TitledTextView = {
        let specialityView = TitledTextView()
        specialityView.title = "Known For:"
        return specialityView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureViews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        selectionStyle = .none
    }
    
    private func configureViews() {
        contentView.addSubview(dogImageView)
        contentView.addSubview(nameLabel)
        contentView.addSubview(lifespanView)
        contentView.addSubview(specialityView)
        
        dogImageView.snp.makeConstraints { make in
            make.width.equalToSuperview()
            make.top.equalToSuperview().offset(16)
            make.left.right.equalToSuperview()
        }
        nameLabel.snp.makeConstraints { make in
            make.top.equalTo(dogImageView.snp.bottom).offset(5)
            make.left.right.equalToSuperview()
        }
        lifespanView.snp.makeConstraints { make in
            make.top.equalTo(nameLabel.snp.bottom).offset(2)
            make.left.right.equalToSuperview()
        }
        specialityView.snp.makeConstraints { make in
            make.top.equalTo(lifespanView.snp.bottom).offset(2)
            make.left.right.equalToSuperview()
            make.bottom.equalToSuperview().offset(-16)
        }
    }
}
