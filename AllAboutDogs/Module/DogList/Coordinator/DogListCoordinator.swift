//
//  DogListCoordinator.swift
//  AllAboutDogs
//
//  Created by Sachin Sawant on 10/7/19.
//  Copyright © 2019 Sachin Sawant. All rights reserved.
//

import UIKit

class DogListCoordinator {
    let window: UIWindow
    var navigationController: UINavigationController?
    
    private lazy var dogsListViewController: DogsListViewController = {
        let dogsViewModel = DogsViewModel()
        let viewController = DogsListViewController(withViewModel: dogsViewModel)
        viewController.delegate = self
        return viewController
    }()
    
    init(with window: UIWindow) {
        self.window = window
    }
    
    public func start() {
        navigationController = UINavigationController(rootViewController: dogsListViewController)
        navigationController?.navigationBar.prefersLargeTitles = true
        window.rootViewController = navigationController
    }
}

extension DogListCoordinator: DogsListViewControllerDelegate {
    func didRequestSettings() {
        //TODO: Add code
    }
}
