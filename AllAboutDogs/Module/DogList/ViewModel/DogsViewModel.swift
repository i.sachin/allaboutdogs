//
//  DogsViewModel.swift
//  AllAboutDogs
//
//  Created by Sachin Sawant on 10/7/19.
//  Copyright © 2019 Sachin Sawant. All rights reserved.
//

import Foundation
import Reachability

protocol DogsViewModelDelegate: class {
    func dogsViewModelDidUpdate(_ viewModel: DogsViewModel)
}

class DogsViewModel {
    
    weak var delegate: DogsViewModelDelegate?
    
    private let url =  URL(string: "https://api.thedogapi.com/v1/images/search?limit=50")!
    private var dogCollection: DogCollection?
    
    //MARK:- Public
    public var dataState: DataState = .empty {
        didSet {
            DispatchQueue.main.async {
                self.delegate?.dogsViewModelDidUpdate(self)
            }
        }
    }
    
    enum SortOption {
        case ascendingByLifespan
        case descendingByLifespan
        case ascendingByLowerLifespan
        case descendingByLowerLifespan
        case ascendingByUpperLifespan
        case descendingByUpperLifespan
    }
    
    public func loadCollection() {
        dataState = .loading
        let reachability = Reachability()!
        if reachability.connection == .none {
            dataState = .error
            self.delegate?.dogsViewModelDidUpdate(self)
            return
        } else {
            let dogAPI = DependencyMap.dogAPIProvider.makeDogAPI()
            dogAPI.fetchDogCollection(sourceURL: url) { [weak self] result in
                guard let self = self
                    else { return }
                DispatchQueue.main.async {
                    switch result {
                    case .success(let dogCollection):
                        self.dogCollection = dogCollection
                        self.dataState = .loaded
                    case .failure:
                        self.dataState = .error
                    }
                }
            }
        }
    }
    
    public func numberOfDogs() -> Int {
        return dogCollection?.dogs.count ?? 0
    }
    
    public func dog(atIndex index: Int) -> Dog? {
        return dogCollection?.dogs[safe: index]
    }
    
    public func sort(byOption option: SortOption) {
        guard let allDogs = dogCollection?.dogs
            else { return }
        dataState = .loading
        var sortedDogs: [Dog]
        switch option {
        case .ascendingByLifespan:
            sortedDogs = allDogs.sorted(by: { (d1, d2) -> Bool in
                d1.breed.averageLifespan < d2.breed.averageLifespan
            })
        case .descendingByLifespan:
            sortedDogs = allDogs.sorted(by: { (d1, d2) -> Bool in
                d1.breed.averageLifespan > d2.breed.averageLifespan
            })
        case .ascendingByLowerLifespan:
            sortedDogs = allDogs.sorted(by: { (d1, d2) -> Bool in
                d1.breed.lifeSpan.lower < d2.breed.lifeSpan.lower
            })
        case .descendingByLowerLifespan:
            sortedDogs = allDogs.sorted(by: { (d1, d2) -> Bool in
                d1.breed.lifeSpan.lower > d2.breed.lifeSpan.lower
            })
        case .ascendingByUpperLifespan:
            sortedDogs = allDogs.sorted(by: { (d1, d2) -> Bool in
                d1.breed.lifeSpan.upper < d2.breed.lifeSpan.upper
            })
        case .descendingByUpperLifespan:
            sortedDogs = allDogs.sorted(by: { (d1, d2) -> Bool in
                d1.breed.lifeSpan.upper > d2.breed.lifeSpan.upper
            })
        }
        let filteredDogs = sortedDogs
            .filter { $0.breed.lifeSpan.lower != 0 && $0.breed.lifeSpan.upper != 0 }
        dogCollection?.dogs = filteredDogs
        dataState = .loaded
        delegate?.dogsViewModelDidUpdate(self)
    }
}
