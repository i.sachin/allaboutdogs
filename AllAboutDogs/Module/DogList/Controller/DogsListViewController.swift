//
//  DogsListViewController.swift
//  AllAboutDogs
//
//  Created by Sachin Sawant on 10/7/19.
//  Copyright © 2019 Sachin Sawant. All rights reserved.
//

import UIKit
import SDWebImage

protocol DogsListViewControllerDelegate: class {
    func didRequestSettings()
}

class DogsListViewController: UIViewController {
    
    weak var delegate: DogsListViewControllerDelegate?
    var viewModel: DogsViewModel!
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.tableFooterView = UIView()
        tableView.estimatedRowHeight = 300
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorColor = UIColor.brown
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        tableView.refreshControl = UIRefreshControl()
        tableView.refreshControl?.addTarget(self, action: #selector(onPullToRefresh), for: .valueChanged)
        tableView.register(DogTableViewCell.self, forCellReuseIdentifier: DogTableViewCell.reuseIdentifier)
        return tableView
    }()
    
    private lazy var sortBarButtonItem: UIBarButtonItem = {
        let barButtonItem = UIBarButtonItem(title: "Sort", style: .plain, target: self, action: #selector(sortButtonTapped))
        return barButtonItem
    }()
    
    private lazy var activityIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.startAnimating()
        return indicator
    }()
    
    private lazy var errorView: ErrorView = {
        let view = ErrorView()
        view.delegate = self
        return view
    }()
    
    init(withViewModel viewModel: DogsViewModel) {
        super.init(nibName: nil, bundle: nil)
        
        self.viewModel = viewModel
        self.viewModel.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "All About Dogs"
        navigationItem.rightBarButtonItem = sortBarButtonItem
        configureViews()
        
        //Fetch dog collection
        viewModel.loadCollection()
    }
    
    private func configureViews() {
        view.addSubview(tableView)
        view.addSubview(activityIndicator)
        
        activityIndicator.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    @objc func sortButtonTapped() {
        let alert = UIAlertController(title: "Select sort option", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Average lifespan Low - High", style: .default, handler: { [weak self] action in
            self?.viewModel.sort(byOption: .ascendingByLifespan)
        }))
        alert.addAction(UIAlertAction(title: "Average lifespan High - Low", style: .default, handler: { [weak self] action in
            self?.viewModel.sort(byOption: .descendingByLifespan)
        }))
        alert.addAction(UIAlertAction(title: "Minium lifespan Low - High", style: .default, handler: { [weak self] action in
            self?.viewModel.sort(byOption: .ascendingByLowerLifespan)
        }))
        alert.addAction(UIAlertAction(title: "Minimum lifespan High - Low", style: .default, handler: { [weak self] action in
            self?.viewModel.sort(byOption: .descendingByLowerLifespan)
        }))
        alert.addAction(UIAlertAction(title: "Maximum lifespan Low - High", style: .default, handler: { [weak self] action in
            self?.viewModel.sort(byOption: .ascendingByUpperLifespan)
        }))
        alert.addAction(UIAlertAction(title: "Maximum lifespan High - Low", style: .default, handler: { [weak self] action in
            self?.viewModel.sort(byOption: .descendingByUpperLifespan)
        }))
        present(alert, animated: true)
    }
    
    @objc func onPullToRefresh() {
        viewModel.loadCollection()
    }
}

extension DogsListViewController: UITableViewDataSource, UITableViewDelegate {
    //MARK:- UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.dataState == .loaded ? 1 : 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfDogs()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: DogTableViewCell.reuseIdentifier, for: indexPath)
        guard let dogTableViewCell = cell as? DogTableViewCell,
            let dog = viewModel.dog(atIndex: indexPath.row)
            else { return cell }
        configureCell(dogTableViewCell, withDog: dog)
        dogTableViewCell.layoutIfNeeded()
        return dogTableViewCell
    }
    
    private func configureCell(_ cell: DogTableViewCell, withDog dog: Dog) {
        cell.name = dog.breed.name
        cell.lifespan = dog.breed.readableLifespan
        cell.speciality = dog.breed.speciality
        cell.imageSize = CGSize(width: Int(dog.image.width), height: Int(dog.image.height))
        cell.dogImageView.sd_setImage(with: URL(string: dog.image.url))
        cell.dogImageView.sd_setImage(with: URL(string: dog.image.url), placeholderImage: UIImage(named: "dog-placeholder"))
    }
}

extension DogsListViewController: DogsViewModelDelegate {
    func dogsViewModelDidUpdate(_ viewModel: DogsViewModel) {
        switch viewModel.dataState {
        case .loading:
            activityIndicator.startAnimating()
        case .loaded:
            activityIndicator.stopAnimating()
            tableView.refreshControl?.endRefreshing()
            tableView.reloadData()
        case .error:
            activityIndicator.stopAnimating()
            tableView.refreshControl?.endRefreshing()
            tableView.backgroundView = errorView
            tableView.reloadData()
        default:
            return
        }
    }
}

extension DogsListViewController: ErrorViewDelegate {
    func errorViewDidTapRetry(_ errorView: ErrorView) {
        tableView.backgroundView = nil
        viewModel.loadCollection()
    }
}
