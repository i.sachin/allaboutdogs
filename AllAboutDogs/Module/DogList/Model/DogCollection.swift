//
//  PetCollection.swift
//  AllAboutDogs
//
//  Created by Sachin Sawant on 10/7/19.
//  Copyright © 2019 Sachin Sawant. All rights reserved.
//

import Foundation

struct DogCollection: Decodable {
    var dogs: [Dog] = []
    
    private struct AnyDecodable: Decodable {}
    
    init(from decoder: Decoder) throws {
        var container = try decoder.unkeyedContainer()
        while !(container.isAtEnd) {
            if let dog = try? container.decode(Dog.self) {
                self.dogs.append(dog)
            } else {
                // Force succeed decode to increment current index
                _ = try? container.decode(AnyDecodable.self)
            }
        }
    }
    
    public func sortedByName() {
    }
}

