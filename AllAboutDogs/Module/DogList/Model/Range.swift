//
//  Range.swift
//  AllAboutDogs
//
//  Created by Sachin Sawant on 10/7/19.
//  Copyright © 2019 Sachin Sawant. All rights reserved.
//

import Foundation

struct Range {
    let lower: UInt
    let upper: UInt
    
    init(withString str: String) {
        let components = str.trimmingCharacters(in: CharacterSet(charactersIn: "0123456789-").inverted)
            .replacingOccurrences(of: " ", with: "")
            .components(separatedBy: "-")
        guard components.count >= 2,
            let lowerRange = UInt(components[0]),
            let upperRange = UInt(components[1])
            else { lower = 0
                upper = 0
                return
        }
        lower = lowerRange
        upper = upperRange
    }
}
