//
//  Dog.swift
//  AllAboutDogs
//
//  Created by Sachin Sawant on 11/7/19.
//  Copyright © 2019 Sachin Sawant. All rights reserved.
//

import Foundation

struct Dog: Decodable {
    let id: String
    var breed: Breed
    let image: Image
    
    private enum CodingKeys: String, CodingKey {
        case id
        case breeds
        case url
        case width
        case height
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        // id
        id = try container.decode(String.self, forKey: .id)
        // breed (read first and throw if fails)
        var breedsContainer = try container.nestedUnkeyedContainer(forKey: .breeds)
        self.breed = try breedsContainer.decode(Breed.self)
        // image
        let url = try container.decode(String.self, forKey: .url)
        let width = try container.decode(UInt.self, forKey: .width)
        let height = try container.decode(UInt.self, forKey: .height)
        self.image = Image(url: url, width: width, height: height)
    }
}

