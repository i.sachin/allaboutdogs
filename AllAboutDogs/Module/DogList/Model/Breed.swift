//
//  Breed.swift
//  AllAboutDogs
//
//  Created by Sachin Sawant on 11/7/19.
//  Copyright © 2019 Sachin Sawant. All rights reserved.
//

import Foundation

struct Breed: Decodable {
    let id: Int
    let name: String
    let height: Measure
    let weight: Measure
    let lifeSpan: Range
    let speciality: String
    let group: String
    let temperament: String
    
    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case height
        case weight
        case lifeSpan = "life_span"
        case speciality = "bred_for"
        case group = "breed_group"
        case temperament
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(Int.self, forKey: .id)
        name = try container.decode(String.self, forKey: .name)
        height = try container.decode(Measure.self, forKey: .height)
        weight = try container.decode(Measure.self, forKey: .height)
        speciality = try container.decode(String.self, forKey: .speciality)
        group = try container.decode(String.self, forKey: .group)
        temperament = try container.decode(String.self, forKey: .temperament)
        let rangeString = try container.decode(String.self, forKey: .lifeSpan)
        lifeSpan = Range(withString: rangeString)
    }
}

extension Breed {
    var readableLifespan: String {
        guard lifeSpan.upper > 0 else {
            return "unknown"
        }
        return lifeSpan.lower == lifeSpan.upper
            ? "\(String(lifeSpan.lower)) years"
            : "\(String(lifeSpan.lower)) - \(String(lifeSpan.upper)) years"
    }
    
    var averageLifespan: Float {
        return Float((lifeSpan.lower + lifeSpan.upper) / 2)
    }
}
