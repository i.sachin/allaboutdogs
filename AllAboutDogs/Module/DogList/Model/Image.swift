//
//  Image.swift
//  AllAboutDogs
//
//  Created by Sachin Sawant on 10/7/19.
//  Copyright © 2019 Sachin Sawant. All rights reserved.
//

import Foundation

struct Image {
    let url: String
    let width: UInt
    let height: UInt
}

