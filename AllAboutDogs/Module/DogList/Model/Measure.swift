//
//  Measure.swift
//  AllAboutDogs
//
//  Created by Sachin Sawant on 10/7/19.
//  Copyright © 2019 Sachin Sawant. All rights reserved.
//

import Foundation

struct Measure: Decodable {
    let imperial: Range
    let metric: Range
    
    private enum CodingKeys: String, CodingKey {
        case imperial
        case metric
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let imperialHeight = try container.decode(String.self, forKey: .imperial)
        let metricHeight = try container.decode(String.self, forKey: .metric)
        imperial = Range(withString: imperialHeight)
        metric = Range(withString: metricHeight)
    }
}
