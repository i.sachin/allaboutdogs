//
//  ViewController.swift
//  AllAboutDogs
//
//  Created by Sachin Sawant on 10/7/19.
//  Copyright © 2019 Sachin Sawant. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let dogAPI = DependencyMap.dogAPIProvider.makeDogAPI()
        let url = URL(string: "https://api.thedogapi.com/v1/images/search?limit=50")!
        dogAPI.fetchDogCollection(sourceURL: url) { result in
            switch result {
            case .success(let dogCollection):
                print(dogCollection)
            case .failure(let error):
                print(error)
            }
        }
    }


}

