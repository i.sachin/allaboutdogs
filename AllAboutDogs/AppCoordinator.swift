//
//  AppCoordinator.swift
//  AllAboutDogs
//
//  Created by Sachin Sawant on 10/7/19.
//  Copyright © 2019 Sachin Sawant. All rights reserved.
//

import UIKit

class AppCoordinator {
    
    let window: UIWindow
    var dogListCoordinator: DogListCoordinator?
    
    init(with window: UIWindow) {
        self.window = window
    }
    
    public func start() {
        showDogList()
    }
    
    private func showDogList() {
        dogListCoordinator = DogListCoordinator(with: window)
        dogListCoordinator?.start()
    }
}
